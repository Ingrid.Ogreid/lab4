package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

public class BriansBrain implements CellAutomaton {
	
	IGrid currentGeneration;

	//Constructor
	public BriansBrain(int rows, int cols) {
		currentGeneration = new CellGrid(rows, cols, CellState.DEAD);
		initializeCells();
	}
	
	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int x = 0; x < numberOfRows(); x++) {
			for (int y = 0; y < numberOfColumns(); y++) {
				nextGeneration.set(y, x, this.getNextCell(y, x));
			}
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState currentState = getCellState(row, col);
		CellState newState = CellState.DEAD;
		
		if (currentState.equals(CellState.ALIVE)) {
			newState = CellState.DYING;
		}
		else if (currentState.equals(CellState.DYING)) {
			newState = CellState.DEAD;
		}
		else {
			if (livingNeighbors == 2) {
				newState = CellState.ALIVE;
			}
		}
		
		return newState;
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
	
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		
		for (int x = col - 1; x < col + 2; x++) {
			for (int y = row - 1; y < row + 2; y++) {
				if (x == col && y == row) {
						continue;
				}
				if (x < numberOfColumns() && y < numberOfRows() && x >= 0 && y >= 0 && getCellState(y, x).equals(state)) {
					count++;
				}
			}
		}
		return count;
		
	}

}
