package cellular;

import java.util.Random;

import datastructure.CellGrid;
import datastructure.IGrid;

/**
 * 
 * A CellAutomata that implements Conways game of life.
 * 
 * @see CellAutomaton
 * 
 *      Every cell has two states: Alive or Dead. Each step the state of each
 *      cell is decided from its neighbors (diagonal, horizontal and lateral).
 *      If the cell has less than two alive Neighbors or more than three
 *      neighbors the cell dies. If a dead cell has exactly three neighbors it
 *      will become alive.
 * 
 * @author eivind
 * @author Martin Vatshelle - martin.vatshelle@uib.no
 * @author Sondre Bolland - sondre.bolland@uib.no
 */
public class GameOfLife implements CellAutomaton {

	/**
	 * The grid of cells
	 */
	IGrid currentGeneration;

	/**
	 * 
	 * Construct a Game Of Life Cell Automaton that holds cells in a grid of the
	 * provided size
	 * 
	 * @param height The height of the grid of cells
	 * @param width  The width of the grid of cells
	 */
	public GameOfLife(int rows, int cols) {
		currentGeneration = new CellGrid(rows, cols, CellState.DEAD);
		initializeCells();
	}

	@Override
	public void initializeCells() {
		Random random = new Random();
		for (int row = 0; row < currentGeneration.numRows(); row++) {
			for (int col = 0; col < currentGeneration.numColumns(); col++) {
				if (random.nextBoolean()) {
					currentGeneration.set(row, col, CellState.ALIVE);
				} else {
					currentGeneration.set(row, col, CellState.DEAD);
				}
			}
		}
	}

	@Override
	public int numberOfRows() {
		return currentGeneration.numRows();
	}

	@Override
	public int numberOfColumns() {
		return currentGeneration.numColumns();
	}

	@Override
	public CellState getCellState(int row, int col) {
		return currentGeneration.get(row, col);
	}

	@Override
	public void step() {
		IGrid nextGeneration = currentGeneration.copy();
		
		for (int x = 0; x < numberOfRows(); x++) {
			for (int y = 0; y < numberOfColumns(); y++) {
				nextGeneration.set(y, x, this.getNextCell(y, x));
			}
		}
		this.currentGeneration = nextGeneration;
	}

	@Override
	public CellState getNextCell(int row, int col) {
		int livingNeighbors = countNeighbors(row, col, CellState.ALIVE);
		CellState currentState = getCellState(row, col);
		CellState newState = CellState.DEAD;
		
		if (currentState.equals(CellState.ALIVE)) {
			if (livingNeighbors == 2 || livingNeighbors == 3) {
				newState = CellState.ALIVE;
			}
		}
		else {
			if (livingNeighbors == 3) {
				newState = CellState.ALIVE;
			}
			
		}
		return newState;
	}

	/**
	 * Calculates the number of neighbors having a given CellState of a cell on
	 * position (row, col) on the board
	 * 
	 * Note that a cell has 8 neighbors in total, of which any number between 0 and
	 * 8 can be the given CellState. The exception are cells along the boarders of
	 * the board: these cells have anywhere between 3 neighbors (in the case of a
	 * corner-cell) and 5 neighbors in total.
	 * 
	 * @param x     the x-position of the cell
	 * @param y     the y-position of the cell
	 * @param state the Cellstate we want to count occurences of.
	 * @return the number of neighbors with given state
	 */
	private int countNeighbors(int row, int col, CellState state) {
		int count = 0;
		
		for (int x = col - 1; x < col + 2; x++) {
			for (int y = row - 1; y < row + 2; y++) {
				if (x == col && y == row) {
						continue;
				}
				if (x < numberOfColumns() && y < numberOfRows() && x >= 0 && y >= 0 && getCellState(y, x).equals(state)) {
					count++;
				}
			}
		}
		return count;
		
	}

	@Override
	public IGrid getGrid() {
		return currentGeneration;
	}
}
