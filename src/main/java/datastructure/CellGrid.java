package datastructure;

import cellular.CellState;

public class CellGrid implements IGrid {
    int rows;
    int cols;
    CellState[][] grid;

    public CellGrid(int rows, int cols, CellState initialState) {
		this.rows = rows;
        this.cols = cols;
        grid = new CellState[cols][rows];
        
        for (int row = 0; row < rows; row++) {
        	for (int col = 0; col < cols; col++) {
        		grid[col][row] = initialState;
        	}
        }
        
	}

    @Override
    public int numRows() {
        return rows;
    }

    @Override
    public int numColumns() {
        return cols;
    }

    @Override
    public void set(int row, int col, CellState element) {
        if ((row > -1) && (row < rows) && (col > -1) && (col < cols)) {
            this.grid[col][row] = element;
        }
        else {
            throw new IndexOutOfBoundsException("Not valid cell index");
        }
        
    }

    @Override
    public CellState get(int row, int col) {
        if ((row > -1) && (row < rows) && (col > -1) && (col < cols)) {
            return this.grid[col][row];
        }
        else {
            throw new IndexOutOfBoundsException("Not valid cell index");
        }
    }
    @Override
    public IGrid copy() {
        CellGrid newGrid = new CellGrid(this.numRows(), this.numColumns(), CellState.DEAD);
        newGrid.grid = this.grid;
        return newGrid;
    }
    
}
